var http = require("http");
var url = require("url");

var portNumber = 11039;

function initServer(router, handlers){
	function onRequest(request, response){

		var reqURL = url.parse(request.url);
		var data = '';
	
		request.setEncoding("utf8");
		
		request.on('data', function(chunk){
			data += chunk;
		});
		
		request.on('end', function(){
			router(handlers, reqURL, response, data);
		});
		
	}
	
	http.createServer(onRequest).listen(portNumber);
	console.log('Server is running on port ' + portNumber + '.')
}

exports.initServer = initServer;