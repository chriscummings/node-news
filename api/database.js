var Sequelize = require('sequelize');
var inArray = require('../shared/helpers/utils');
var _ = require('underscore');
// Config Includes
var creds = require('../shared/database/credentials').creds.database;
var schema = require('../shared/database/schema').schema

var sequelize;
var NewsItem;

function databaseInit(){
	sequelize = new Sequelize(creds.database, creds.user, creds.pass, {
		host: "localhost",
		logging: false
	});
	
	NewsItem = schema.NewsItem(sequelize, Sequelize);
	Message = schema.IRC_Message(sequelize, Sequelize);
};

function buildMessageWhereString(){
};


function buildWebsiteWhereString(array){
	var SQLwhereFragments = [];
	for(item in array){
		SQLwhereFragments.push('website = "' + array[item] + '"');
	};
	return SQLwhereFragments.join(' OR ');
};

function buildMessageWhereString(array){
	var SQLwhereFragments = [];
	for(item in array){
		SQLwhereFragments.push('user = "' + array[item] + '"');
	};
	return SQLwhereFragments.join(' OR ');
};


function getMessages(query, response, callback){
	
	var queryParams = {};
	queryParams.limit = query['limit'];
	queryParams.offset = query['count'];
	queryParams.order = 'time DESC';
	
	if(query['user']){
		var sitesToQuery = query['user'].toLowerCase().split(' ');
		queryParams.where = buildMessageWhereString(sitesToQuery);	
	};
	
	// TODO: implement between x and y times
	// FIXME: if the comparison is in caps in the querystring this probably 
	// breaks
	if(query['time']){
		
		// Handle any comparison operators
		var comparisonOperator = '';
				
		if( query['time'].toLowerCase().indexOf('gt') !== -1 ){
			comparisonOperator = '>';
			if(query['time'].toLowerCase().indexOf('eq') !== -1 ){
				comparisonOperator += '=';
			};
			
		}else if( query['time'].toLowerCase().indexOf('lt') !== -1 ){
			comparisonOperator = '<';
			if(query['time'].toLowerCase().indexOf('eq') !== -1 ){
				comparisonOperator += '=';
			};
			
		}else{
			comparisonOperator = '=';
		};
		
		// Remove any comparison operators from query string
		// FIXME: make this case insensitive!
		query['time'] = query['time']
			.replace("eq", " ")
			.replace("lt", " ")
			.replace("gt", " ")


		if(queryParams.where){
			queryParams.where += ' AND time '+comparisonOperator+' "'+query['time']+'"'
		}else{
			queryParams.where = 'time '+comparisonOperator+' "'+query['time']+'"'
		};
	};
		
	Message.findAll(queryParams)
	.on('success', function(items){
		callback(items, response);
	})
	.on('failure', function(error){
		console.log('ERROR: ', error)
	});

};

function getNews(query, response, callback){
	// FIXME: get this from websites table...
	var allSites = [
		'reddit', 
		'github', 
		'delicious', 
		'google groups', 
		'stack overflow',
		'twitter',
		'digg'
	];
	
	var queryParams = {};
	
	if(query['only']){
		
		var sitesToQuery = query['only'].toLowerCase().split(' ');
		queryParams.where = buildWebsiteWhereString(sitesToQuery);
		
	}else if(query['exclude']){
		
		var sitesToExclude = query['exclude'].toLowerCase().split(' ');
		var sitesToQuery = _.reject(allSites, function(site){
			return inArray(site, sitesToExclude);
		});
		queryParams.where = buildWebsiteWhereString(sitesToQuery);		
	};
	
	queryParams.limit = query['limit'];
	queryParams.offset = query['count'];
	queryParams.order = 'created DESC';
	
	NewsItem.findAll(queryParams)
	.on('success', function(items){
		console.log(items)
		callback(items, response);
	})
	.on('failure', function(error){
		console.log('ERROR: ', error)
	});
};

exports.databaseInit = databaseInit;
exports.getNews = getNews;
exports.getMessages =getMessages;