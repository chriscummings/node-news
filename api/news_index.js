var server = require('./server');
var router = require('./router');
var requestHandlers = require('./requesthandlers');
requestHandlers.initDatabaseConnection();

var handlers = {};
handlers['/'] = requestHandlers.root;
handlers['/irc'] = requestHandlers.irc;


server.initServer(router.router, handlers);