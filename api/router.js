function router(handlers, url, response, data){
	var pathname = url.pathname
	
	console.log('About to route: '+ pathname)
	
	if(typeof handlers[pathname] == 'function'){
		handlers[pathname](url, response, data);
	} else {
		console.log("No handler for " + pathname);
		response.writeHead(404, {"Content-Type": "text/html"});
	    response.write('404 Not found. ("'+pathname+'")');
	    response.end();
	}
	
}

exports.router = router;