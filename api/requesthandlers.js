var querystring = require('querystring');
var database = require('./database');

function initDatabaseConnection(){
	database.databaseInit();
};

// TODO: Is this the best place for DB to JSON conversion function?
function JSONResponse(results, response){
	
	var json = [];
	
	for(var i=0, r=results.length ; i<r; i++){
		var result = results[i];	

		var itemHash = {};

		for(var j=0, a=result.attributes.length; j<a; j++){
			var attribute = result.attributes[j] + '';

			itemHash[attribute] = result[attribute]
		};
		
		itemHash["metadata"] = JSON.parse(itemHash["metadata"]);
		json.push(itemHash);
	
		// json.push({
		// 	"title": result.title ,
		// 	"website": result.website,
		// 	"subdomain": result.subdomain,
		// 	"type": result.type,
		// 	"url": result.url,
		// 	"refUrl":result.refUrl,
		// 	"spam":result.spam,
		// 	"verified":result.verified,
		// 	"metadata": JSON.parse(result.metadata), // ???
		// 	"date": result.createdAt
		// });
	};
	
	try{
		json = JSON.stringify(json);		
	}catch(SyntaxError){
		console.log('Bad Syntax');
		return false;
	};

	response.writeHead(200, {
		'Content-Type': 'application/json',
		'Access-Control-Allow-Origin' : '*' // Added to use localhost 
	});
	response.end(json);
};

function messagesToJSON(results, response){
	var json = [];
		
	// FIXME: empty results????
	for(var i=0; i<results.length; i++){
		
		var result = results[i];

		json.push({
			"user": result.user ,
			"text": result.text,
			'time': result.time,
		});
			
	};

	try{
		json = JSON.stringify(json);
	}catch(SyntaxError){
		console.log('Bad Syntax');
		return false;
	};

	response.writeHead(200, {
		'Content-Type': 'application/json',
		'Access-Control-Allow-Origin' : '*' // Added to use localhost 
	});
	response.end(json);
}

function root(url, response, data){
	console.log('Handler for query called.')
	
	if(url.query){
		var query = querystring.parse(url.query);
				
		query['count'] = query['count'] || 0;
		query['limit'] = query['limit'] || 20;
		// Also supports "only" and "exclude"

		database.getNews(query, response, JSONResponse);
	}else{
		database.getNews({
			'count':0,
			'limit':50
		}, response, JSONResponse);
	};
	
};

function irc(url, response, data){
	console.log('Handler for irc called.')
	
	if(url.query){
		var query = querystring.parse(url.query);
				
		query['count'] = query['count'] || 0;
		query['limit'] = query['limit'] || 20;
		// Also supports "only" and "exclude"
		
		database.getMessages(query, response, messagesToJSON);
	}else{
		database.getMessages({
			'count':0,
			'limit':50
		}, response, messagesToJSON);
	};	
}

exports.irc = irc;
exports.root = root;

exports.initDatabaseConnection = initDatabaseConnection;