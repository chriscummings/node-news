schema = {}

schema.IRC_Message = function(sequelize, Sequelize){
	return sequelize.define('IRC_Message', {
		user: {type: Sequelize.STRING, allowNull: false},
		time: {type: Sequelize.DATE, allowNull: false},
		text: {type: Sequelize.TEXT}
	},
	{
		timestamps:false // Sequelize uses localtime for timestamps
	});
};

schema.NewsItem = function(sequelize, Sequelize){
	return sequelize.define('NewsItem', {
		title: { type: Sequelize.STRING, allowNull: false},
		url: {type: Sequelize.STRING, unique: true, allowNull: false},
		refUrl: {type: Sequelize.STRING, unique: true, allowNull: false},
		website: {type: Sequelize.STRING, allowNull: false},
		displayName: {type: Sequelize.STRING, allowNull: false},
		subdomain: {type: Sequelize.STRING},
		type: {type: Sequelize.STRING},
		metadata: {type: Sequelize.TEXT},
		spam: {type: Sequelize.INTEGER},
		verified: {type: Sequelize.BOOLEAN},
		created: {type: Sequelize.DATE, allowNull: false},
		updated: {type: Sequelize.DATE}
	},
	{
		timestamps:false // Sequelize uses localtime for timestamps
	});
};

schema.Websites = function(sequelize, Sequelize){
	return sequelize.define('Websites', {
		name: { type: Sequelize.STRING, allowNull: false}
	});
};

exports.schema = schema;

