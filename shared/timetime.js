function Timetime(){
	
	this.TIME_IN_SECOND = 1000;
	this.TIME_IN_MINUTE = this.TIME_IN_SECOND * 60;
	this.TIME_IN_HOUR = this.TIME_IN_MINUTE * 60;
	this.TIME_IN_DAY = this.TIME_IN_HOUR * 24;
	this.TIME_IN_WEEK = this.TIME_IN_DAY * 7;
	this.TIME_IN_MONTH = this.TIME_IN_WEEK * 30;
	this.TIME_IN_YEAR = this.TIME_IN_MONTH * 12;
	
}

Timetime.prototype.timeFrom = function(date1, date2, returnType){
	
	var returnType = returnType.toLowerCase(); 
	
	var delta = Math.floor( (date1-date2) );
	
	switch(returnType){
		case 'fancytime':		
			return this.fancifyTime(delta);
			
		case 'fancydate':		
			return this.fancifyDate(delta);

		case 'years':
			return Math.floor(delta/this.TIME_IN_YEAR);

		case 'months':
			return Math.floor(delta/this.TIME_IN_MONTH);

		case 'weeks':
			return Math.floor(delta/this.TIME_IN_WEEK);

		case 'days':
			return Math.floor(delta/this.TIME_IN_DAY);

		case 'hours':
			return Math.floor(delta/this.TIME_IN_HOUR);

		case 'minutes':
			return Math.floor(delta/this.TIME_IN_MINUTE);

		case 'seconds':
			return Math.floor(delta/this.TIME_IN_SECOND);

		default:
			return Math.floor(delta);
	}
};

Timetime.prototype.compareYear = function compareYear(date1, date2){
	return this.compare( date1.getFullYear(), date2.getFullYear() );
};

Timetime.prototype.compareMonth = function compareMonth(date1, date2){
	return this.compare( date1.getMonth(), date2.getMonth() );
};

Timetime.prototype.compareDate = function compareDate(date1, date2){
	return this.compare( date1.getDate(), date2.getDate() );
};

Timetime.prototype.compareHour = function compareHour(date1, date2){
	return this.compare( date1.getHours(), date2.getHours() );
};

Timetime.prototype.compareMinute = function compareMinute(date1, date2){
	return this.compare( date1.getMinutes(), date2.getMinutes() );
};

Timetime.prototype.compareSecond = function compareSecond(date1, date2){
	return this.compare( date1.getSeconds(), date2.getSeconds() );
};

Timetime.prototype.compareMillisecond = function compareMillisecond(date1, date2){
	return this.compare( date1.getMilliseconds(), date2.getMilliseconds() );
};

Timetime.prototype.compare = function(int1, int2){
	
	if(int1 > int2){
		return 1;
	}else if(int1 < int2){
		return -1;
	}else if(int1 === int2){
		return 0;
	}
}

/*
	Compares two dates to a varying level of percision. Returns 1, 0 or -1
*/
Timetime.prototype.compareTo = function(date1, date2, degreeOfPercision){
	var degreeOfPercision = degreeOfPercision.toLowerCase() || 'milliseconds';
		
	var comparison;
	
	comparison = this.compareYear(date1, date2);
	
	if(degreeOfPercision === 'year' || comparison !== 0){
		return comparison;
	}
	
	comparison = this.compareMonth(date1, date2);
	
	if(degreeOfPercision === 'month' || comparison !== 0){
		return comparison;
	}
	
	comparison = this.compareDate(date1, date2);
	
	if(degreeOfPercision === 'date' || comparison !== 0){
		return comparison;
	}
	
	comparison = this.compareHour(date1, date2);
	
	if(degreeOfPercision === 'hour' || comparison !== 0){
		return comparison;
	}

	comparison = this.compareMinute(date1, date2);
	
	if(degreeOfPercision === 'minute' || comparison !== 0){
		return comparison;
	}

	comparison = this.compareSecond(date1, date2);
	
	if(degreeOfPercision === 'second' || comparison !== 0){
		return comparison;
	}
	
	comparison = this.compareMillisecond(date1, date2);
	
	if(degreeOfPercision === 'millisecond' || comparison !== 0){
		return comparison;
	}
	
}

Timetime.prototype.isYesterday = function(then){
	var now = new Date();
	
	if(now.getMonth() === then.getMonth() &&
		now.getYear() === then.getYear() &&
		now.getDate() === then.getDate()+1 ){
			return true;
		}else{
			return false;
		}
};


Timetime.prototype.daysDifference = function(date1, date2){
	var delta = date1 - date2;
	return Math.floor(delta/this.TIME_IN_DAY);
};



Timetime.prototype.fancifyTime = function(miliseconds){
	
	function getPluralString(int){
		if(int > 1){
			return 's';
		}
		return '';
	}

	var ageString = '';

	// TODO: handle negative values so future/past don't matter

	if(miliseconds > this.TIME_IN_YEAR){

		var years = Math.floor(miliseconds/this.TIME_IN_YEAR);
		ageString = years + ' year'+getPluralString(years);

	}else if(miliseconds > this.TIME_IN_MONTH){

		var months = Math.floor(miliseconds/this.TIME_IN_MONTH);
		ageString = months + ' month'+getPluralString(months);

	}else if(miliseconds > this.TIME_IN_WEEK){

		var weeks = Math.floor(miliseconds/this.TIME_IN_WEEK);
		ageString = weeks + ' week'+getPluralString(weeks);

	}else if(miliseconds > this.TIME_IN_DAY){

		var days = Math.floor(miliseconds/this.TIME_IN_DAY);
		ageString = days + ' day'+getPluralString(days);

	}else if(miliseconds > this.TIME_IN_HOUR){

		var hours = Math.floor(miliseconds/this.TIME_IN_HOUR);
		ageString = hours + ' hour'+getPluralString(hours);

	}else if(miliseconds > this.TIME_IN_MINUTE){

		var minutes = Math.floor(miliseconds/this.TIME_IN_MINUTE);
		ageString = minutes + ' minute'+getPluralString(minutes);

	}else{
		// FIXME: resulting in NaN seconds???
		var seconds = Math.floor(miliseconds/this.TIME_IN_SECONDS);
		ageString = seconds + ' second'+getPluralString(seconds);

	}

	return ageString;
};



// var now = new Date();
// var then = Date.parse('5/30/2011');
// var x = new timetime()
// x.timeFrom(then, now, 'fancy');
// 
// 














