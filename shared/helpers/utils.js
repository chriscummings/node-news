var http = require('http');
var https = require('https');

// FIXME: needs error handling
function fetch(opts, https_, callback){

	function handleResponse (response){
		var data = '';

		response.setEncoding('utf8');

		response.on('data', function(d){
			data += d;
		});
		response.on('end', function(){
			callback(data);
		});
	}
	
	
	// Some services require https. GitHub for example.
	var req = ( https_ ? 
		https.request(opts, handleResponse) : 
		http.request(opts, handleResponse) );

	req.end();
}
exports.fetch = fetch;


function UTM_MySQL_Timestamp(){
	var now = new Date();
	var yr = now.getUTCFullYear()
	var mo = now.getUTCMonth()
	var da = now.getUTCDate()
	var hr = now.getUTCHours()
	var mn = now.getUTCMinutes()
	var sc = now.getUTCSeconds()

	when = time = [
		[
		    yr,
		    ( mo <  9 ? '0' : '' ) + ( mo + 1 ),
		    ( da < 10 ? '0' : '' ) + da
		].join('-'), 
		[
		    ( hr < 10 ? '0' : '' ) + hr,
		    ( mn < 10 ? '0' : '' ) + mn,
		    ( sc < 10 ? '0' : '' ) + sc
		].join(':')
	].join(' ');
	
	return when;
}
exports.UTM_MySQL_Timestamp = UTM_MySQL_Timestamp;




function inArray(obj, array){
	var hash = {};
	
	for(var i=0; i<array.length; i++){
		hash[array[i]] = null;
	};
	
	if(obj in hash){
		return true;
	}else{
		return false;
	};
};
exports.inArray = inArray;