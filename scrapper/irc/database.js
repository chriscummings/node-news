var Sequelize = require('sequelize');
// var _ = require('underscore'); // ?
var creds = require('../../shared/database/credentials').creds.database;
var schema = require('../../shared/database/schema').schema;

var Message;
var sequelize;

function databaseInit(){

	console.log('DB INIT')

	sequelize = new Sequelize(creds.database, creds.user, creds.pass, {
		host: "localhost",
		logging: false
	});
	
	IRC_Message = schema.IRC_Message(sequelize, Sequelize);

	IRC_Message.sync({force:false}).on('success', function(){
		console.log('Table Synced');
	}).on('failure', function(error){
		console.log('ERROR: ' + error);
	});

};

function addMessage(user, message, time){
	
	var message = IRC_Message.build({
		user:user,
		time:time,
		text:message
	});
	
	message.save().
		on('success', function(){
			// console.log('Message Saved.');
		}).
		on('failure', function(err){
			// TODO: Handle this better
			console.log('ERROR: ' + err);
		});	

};

exports.addMessage = addMessage;
exports.databaseInit = databaseInit;