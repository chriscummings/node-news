var cron = require('cron');

var Reddit = require('./sites').Reddit;
var GithubCommits = require('./sites').GithubCommits;
var GithubIssues = require('./sites').GithubIssues;
var Twitter = require('./sites').TwitterSearch;
var Delicious = require('./sites').Delicious;
var GoogleGroups = require('./sites').GoogleGroups;
var Digg = require('./sites').DiggSearch;
var StackOverflow = require('./sites').StackOverflow;


new cron.CronJob('5 * * * * *', function(){
	update();
});

// call right away on init
update();

function update(){
	
	// FIXME: step through theses instead of concurently
	
	var stackOverflow = new StackOverflow('questions?tagged=node.js');
	stackOverflow.update();
	
	var r_node = new Reddit('node');
	r_node.update();	
	
	//Github
	var node_commits = new GithubCommits('joyent', 'node', 'master');
	node_commits.update();	
	
	var node_issues = new GithubIssues('joyent', 'node');
	node_issues.update();	
	
	// Google Groups
	var googleGroup = new GoogleGroups('nodejs');
	googleGroup.update();
	
	var googleGroup_dev = new GoogleGroups('nodejs-dev');
	googleGroup_dev.update();
	
	// Delicious
	var delicious_node_dot_js = new Delicious('node.js');
	delicious_node_dot_js.update();
	
	var delicious_nodejs = new Delicious('nodejs');
	delicious_nodejs.update();
	
	// Twitter
	var tweet_nodehashtag = new Twitter('%23nodejs'); // hashtag
	tweet_nodehashtag.update();
	
	var tweet_nodejs = new Twitter('nodejs');
	tweet_nodejs.update();
	
	var tweet_node_dot_js = new Twitter('node.js');
	tweet_node_dot_js.update();
	
	// Digg
	var digg = new Digg('nodejs');
	digg.update();
	
	var digg = new Digg('node.js');
	digg.update();

};

