var Sequelize = require('sequelize');
var _ = require('underscore');

var inArray = require('../../shared/helpers/utils');
var creds = require('../../shared/database/credentials').creds.database;
var schema = require('../../shared/database/schema').schema
var UTM_MySQL_Timestamp = require('../../shared/helpers/utils').UTM_MySQL_Timestamp;


var NewsItem;
var sequelize;
var Websites;

function databaseInit(){

	console.log('Init Database')

	sequelize = new Sequelize(creds.database, creds.user, creds.pass, {
		host: "localhost",
		logging: false
	});
	
	NewsItem = schema.NewsItem(sequelize, Sequelize);
	Websites = schema.Websites(sequelize, Sequelize);

	var forceSync = false
	
	NewsItem.sync({force:forceSync})
		.on('success', function(){
			console.log('Table Synced')
		})
		.on('failure', function(error){
			console.log('ERROR: ' + error)
		});
	
	Websites.sync({force:forceSync})
		.on('success', function(){
			console.log('Table Synced')
		})
		.on('failure', function(error){
			console.log('ERROR: ' + error)
		});
		
};

function addNewsItems(items){	
	
	if(items.length < 1){
		return;
	}
		
	var lastItem = items.pop();
	
	var when = UTM_MySQL_Timestamp();
	
	var attributes = {
		title:lastItem.title,
		url:lastItem.url,
		refUrl:lastItem.refUrl,
		website:lastItem.website,
		displayName:lastItem.displayName,
		subdomain:lastItem.subdomain,
		type:lastItem.type,
		metadata:JSON.stringify(lastItem.metadata),
		created: when,
		updated: when
	};
	
	
	var newsItem = NewsItem.build(attributes);
		
	NewsItem.find({
		where: {url:lastItem.url}
	}).
	on('success', function(newsItems){
		if(newsItems){
			
			// Update attributes if exists
			newsItems.updateAttributes(attributes)
				.on('success', function(){
					console.log('Updating')
				})
				.on('failure', function(err){
					console.log('ERROR: ' + err);
				});
			
			// Recursively call self with 1-shorter items array.			
			addNewsItems(items);
		
		}else{
			
			console.log('saving')
			newsItem.save()
				.on('success', function(){
					console.log('NewsItem Saved.');
					addNewsItems(items);
				})
				.on('failure', function(err){
					// TODO: Handle this better
					console.log('ERROR: ' + err);
				});
		};
	
	}).
	on('failure', function(e){
		console.log('Error: ' + e)
	});

}


exports.addNewsItems = addNewsItems;
exports.databaseInit = databaseInit;

