var fetch = require('../../shared/helpers/utils').fetch;
var xml2js = require('xml2js');
var fs = require('fs');
var sys = require('sys')
var exec = require('child_process').exec;
var child;

function Scraper(obj){
	if(!obj.host){
		throw('No host argument.');
	}
	
	this.opts = {
		host: obj.host,
		port: obj.port || '80',
		path: obj.path || '',
		method: obj.method || 'GET',
		https: obj.https || false,
		isXml: obj.isXml || false,
		gzipped: obj.gzipped || false
	};
};

// Second argument keeps "this" context
Scraper.prototype.getJSON = function(callback, context){	
	
	var error = null;
	var json = null;

	// Preserve context between objects
	var self = this;

	if(this.opts.gzipped){
			var fileName = new Date().getTime() + '.json';
			var url = this.opts.host + this.opts.path;
			var curlFlags = ' -o ' + fileName
			
			// Curl the file down
			child = exec("cd ./tmp | curl " + url + ' ' + curlFlags + '.gz', function (error, stdout, stderr) {
				if (error !== null) {
					console.log('exec error: ' + error);
					console.log('stderr: ' + stderr);
					return;
				}
				
				// Uncompress
				child = exec('gunzip -d ' + fileName, function (error, stdout, stderr) {
					if (error !== null) {
						console.log('exec error: ' + error);
						console.log('stderr: ' + stderr);
						return;
					}
					
					// Read file
					fs.readFile(fileName, 'utf8', function(err, file){
						if (error !== null) {
							console.log('exec error: ' + error);
							console.log('stderr: ' + stderr);
							return;
						}
						
						// Delete files
						fs.unlink(__dirname +'/'+ fileName);
						fs.unlink(__dirname +'/'+ fileName + '.gz');						
						
						json = ( JSON.parse(file) );
						callback(error, json, context);
					});
					
				});
			});
		
	}else{
		
		fetch(this.opts, this.opts.https, function(data){
			if(self.opts.isXml){
				var parser = new xml2js.Parser();

				parser.addListener('end', function(result) {
				    json = result.channel.item;
					callback(error, json, context);
				});

				parser.parseString(data);
			}else{
				json = ( JSON.parse(data) );
				callback(error, json, context);
			};

		});
			
	};


};


module.exports = Scraper