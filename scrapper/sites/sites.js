var right = require('../../shared/lib/right-server');
var Scraper = require('./scraper');


// TODO: Is this the right place for database?
var database = require('./database')
database.databaseInit();


// Base class for all websites
var Website = new right.Class({
	initialize:function(connOptions){
		this.scraper = new Scraper(connOptions)
	}, 
	getJSON: function(callback){
		// Give argument of "this" to provide context
		this.scraper.getJSON(callback, this);
	},
	updateDB: function(json){		
		database.addNewsItems(json);
	},
	// formatForDB MUST be overridden to prep each website's own response
	formatForDB: function(error, json){
		throw('Method formatForDB not overridden.')
	},
	update: function(){
		this.getJSON(this.formatForDB)
	}
});


// Template Subclass
// ===========================================================================
var SomeWebsite = new right.Class(Website, {
	initialize:function(someArguments){ // Args here...
		this.$super({ // Connection details here....
			host: '',
			path: '' 
		});
	},
	
	// Formats response from website to be entered into the database.
	// context argument preserves and acts as "this".
	formatForDB: function(error, json, context){
		if(error){
			console.log(error);
			return;
		};
		
		var json = json // Drill down to items array if needed
		
		if(!json){
			return;
		};
		
		var posts = [];

		for(var i=0; i<json.length; i++){
			var item = json[i]; // Drill down if needed
			
			posts.push({
				title: '', //
				url: '', //
				refUrl: '', //
				website: '', //
				displayName: '', //
				subdomain: '', //
				type: '', //
				metadata: item
			});
		};
		
		context.updateDB(posts);
	}

});
// var x = new SomeWebsite('args') // Some args...
// x.update();


// StackOverflow
// ===========================================================================
var StackOverflow = new right.Class(Website, {
	initialize:function(queryString){ // Args here...
		this.$super({ // Connection details here....
			host: 'api.stackoverflow.com',
			path: '/1.1/' + queryString,
			gzipped: true
		});
	},
		
	formatForDB: function(error, json, context){
		if(error){
			console.log(error);
			return;
		};

		var json = json.questions;
		
		if(!json){
			return;
		};
		
		
		var posts = [];
		
		for(var i=0; i<json.length; i++){
			var item = json[i];
			
			var questionID = (item.question_timeline_url).split('/')[2]
			var url = 'http://stackoverflow.com/questions/' + questionID
			
			posts.push({
				title: item.title,
				url: url,
				refUrl: url,
				website: 'stackoverflow',
				displayName: 'Stack Overflow',
				subdomain: '',
				type: 'Question',
				metadata: item
			});
		};
		
		context.updateDB(posts);
	}

});
// var so = new StackOverflow('questions/unanswered?tagged=node.js');
// so.update();
exports.StackOverflow = StackOverflow


// Twitter
// ===========================================================================
var TwitterSearch = new right.Class(Website, {
	initialize:function(queryString){ // Args here...
		this.$super({ // Connection details here....
			host: 'search.twitter.com',
			path: '/search.json?q=' +  queryString
		});
		
		this.queryString = queryString;
	},
		
	formatForDB: function(error, json, context){
		if(error){
			console.log(error);
			return;
		};
		
		var json = json.results;
		
		if(!json){
			return;
		};
		
		
		var posts = [];

		for(var i=0; i<json.length; i++){
			var item = json[i];
			
			posts.push({
				title: item.text,
				url: 'https://twitter.com/#!/'+item.from_user+'/status/' + item.id_str,
				refUrl: 'https://twitter.com/#!/'+item.from_user+'/status/' + item.id_str,
				website: 'twitter',
				displayName: 'Twitter',
				subdomain: '',
				type: 'Tweet',
				metadata: item
			});
		};
		
		context.updateDB(posts);
	}

});
// var t = new TwitterSearch('%23nodejs') // Some args...
// t.update();
exports.TwitterSearch = TwitterSearch


// Digg
// ===========================================================================
var DiggSearch = new right.Class(Website, {
	initialize:function(queryString){
		this.$super({
			host: 'services.digg.com',
			path: '/2.0/search.search?count=99&query=' + queryString 
		});
		
		this.queryString = queryString;
	},

	formatForDB: function(error, json, context){
		if(error){
			console.log(error);
			return;
		};
		
		var json = json.stories;
		
		if(!json){
			return;
		};
		
		var posts = [];

		for(var i=0; i<json.length; i++){
			var item = json[i];
			
			// Try to filter the crap from Digg by requiring >1 diggs
			if(item.diggs > 1){
				posts.push({
					title: item.description,
					url: item.link,
					refUrl: item.href,
					website: 'digg',
					displayName: 'Digg',
					subdomain: '',
					type: '',
					metadata: item
				});	
			}
			
		};
		
		context.updateDB(posts);
	}

});
// var digg = new DiggSearch('nodejs') // Some args...
// digg.update();
exports.DiggSearch = DiggSearch


// Google Groups
// ===========================================================================
var GoogleGroups = new right.Class(Website, {
	initialize:function(group){
		this.$super({
			host: 'groups.google.com',
			path: '/group/'+group+'/feed/rss_v2_0_topics.xml?num=50',
			isXml: true
		});
		
		this.group = group;
	},
	
	formatForDB: function(error, json, context){
		if(error){
			console.log(error);
			return;
		};

		var json = json;
		
		if(!json){
			return;
		};
		
		var posts = [];

		for(var i=0; i<json.length; i++){
			var item = json[i];
			
			posts.push({
				title: item.title,
				url: item.link,
				refUrl: item.link,
				website: 'googlegroups',
				displayName: 'Google Groups',
				subdomain: 'group: ' + context.group,
				type: 'Forum Topic',
				metadata: item
			});
		};
		
		context.updateDB(posts);
	}

});
// var g = new GoogleGroups('nodejs')
// g.update();
exports.GoogleGroups = GoogleGroups


// Delicious
// ===========================================================================
var Delicious = new right.Class(Website, {
	initialize:function(tag){
		this.$super({
			host: 'feeds.delicious.com',
			path: '/v2/json/tag/' + tag
		});
		
		this.tag = tag;
	},
	
	formatForDB: function(error, json, context){
		if(error){
			console.log(error);
			return;
		};
		
		var json = json;
		
		if(!json){
			return;
		};
		
		var posts = [];

		for(var i=0; i<json.length; i++){
			var item = json[i];

			posts.push({
				title: item.d,
				url: item.u,
				refUrl: item.u,
				website: 'delicious',
				displayName: 'Delicious',
				subdomain: 'tag: ' + context.tag,
				type: 'link',
				metadata: item
			});
		};
		
		context.updateDB(posts);
	}

});
// var d = new Delicious('Node.js')
// d.update();
exports.Delicious = Delicious


// Reddit
// ===========================================================================
var Reddit = new right.Class(Website, {
	initialize:function(subreddit){
		this.$super({
			host: 'www.reddit.com',
			path: '/r/'+subreddit+'/new/.json?sort=new'
		});
	
		this.subreddit = subreddit;
	},
	
	formatForDB: function(error, json, context){
		if(error){
			console.log(error);
			return;
		};

		var json = json.data.children;
		
		if(!json){
			return;
		};
		
		var posts = [];

		for(var i=0; i<json.length; i++){
			var item = json[i].data;
						
			posts.push({
				title: item.title,
				url: item.url,
				refUrl: 'http://www.reddit.com' + item.permalink,
				website: 'reddit',
				displayName: 'Reddit',
				subdomain: 'subreddit: r/' + context.subreddit,
				type: '',
				metadata: item
			});
		};
		
		context.updateDB(posts);
	}

});
// var r = new Reddit('node')
// r.update();
exports.Reddit = Reddit


// Github Repo Commits
// ===========================================================================
var GithubCommits = new right.Class(Website, {
	initialize:function(author, repo, branch){
		this.author = author;
		this.repo = repo;
		this.branch = branch;
		
		this.$super({
			host: 'github.com',
			port: '443',
			path: '/api/v2/json/commits/list/'+author+'/'+repo+'/'+branch,
			https: true
		});
	},
	
	formatForDB: function(error, json, context){
		if(error){
			console.log(error);
			return;
		};

		var json = json.commits;
		
		if(!json){
			return;
		};
					
		var posts = [];
		
		for(var i=0; i<json.length; i++){
			var item = json[i];

			posts.push({
				title: item.message,
				url: 'http://www.github.com' + item.url,
				refUrl: 'http://www.github.com' + item.url,
				website: 'github',
				displayName: 'Github',
				subdomain: 'repo: ' + context.author+'/'+context.repo+'/'+context.branch,
				type: 'commit',
				metadata: item
			});
		};
		
		context.updateDB(posts);
	}

});
// var git = new Github('joyent', 'node', 'master')
// git.update();
exports.GithubCommits = GithubCommits

// Github Repo Issues
// ===========================================================================
var GithubIssues = new right.Class(Website, {
	initialize:function(author, repo, branch){
		this.author = author;
		this.repo = repo;
		//this.branch = branch;
		
		this.$super({
			host: 'api.github.com',
			port: '443',
			path: '/repos/'+author+'/'+repo+'/issues',
			https: true
		});
	},
	
	
	formatForDB: function(error, json, context){
		if(error){
			console.log(error);
			return;
		};

		var json = json;
		
		if(!json){
			return;
		};
					
		var posts = [];
		
		for(var i=0; i<json.length; i++){
			var item = json[i];

			posts.push({
				title: item.title,
				url: item.html_url,
				refUrl: item.html_url,
				website: 'github',
				displayName: 'Github',
				subdomain: 'repo: ' + context.author+'/'+context.repo+'',
				type: 'open issue',
				metadata: item
			});
		};
		
		context.updateDB(posts);
	}

});
// var git = new Github('joyent', 'node', 'master')
// git.update();
exports.GithubIssues = GithubIssues