function Timetime(){
	// Constants
	this.TIME_IN_SECOND = 1000;
	this.TIME_IN_MINUTE = this.TIME_IN_SECOND * 60;
	this.TIME_IN_HOUR = this.TIME_IN_MINUTE * 60;
	this.TIME_IN_DAY = this.TIME_IN_HOUR * 24;
	this.TIME_IN_WEEK = this.TIME_IN_DAY * 7;
	this.TIME_IN_MONTH = this.TIME_IN_WEEK * 30;
	this.TIME_IN_YEAR = this.TIME_IN_MONTH * 12;
};

// Takes Date(), date string or ms and returns time in milliseconds.
Timetime.prototype.convertToMS = function(arg){
	if(arg.constructor !== Number){
		if(arg.constructor === String){ return Date.parse(arg); };
		
		if(arg.constructor === Date){ return arg.getTime(); };
	}else{ return arg; };
};

// Takes date string or ms and returns a Date object
Timetime.prototype.convertToDate = function(arg){
	if(arg.constructor === String){ return new Date( Date.parse(arg) ); };
	
	if(arg.constructor === Number){ return new Date(arg); };
	
	if(arg.constructor === Date){ return arg; };
};

// Returns ms between to dates
Timetime.prototype.getDelta = function(date1, date2){
	date1 = this.convertToMS(date1);
	date2 = this.convertToMS(date2);
	
	return date1-date2;
};

// Returns milliseconds +/- between two points of time.
Timetime.prototype.timeBetween = function(date1, date2){
	return this.getDelta(date1, date2);
};


// Returns milliseconds +/- from when function is called.
Timetime.prototype.timeFromNow = function(date1){
	return this.timeBetween(date1, new Date);
};

// Compares two dates to a varying level of percision. Returns 1, 0 or -1
Timetime.prototype.compareTo = function(date1, date2, degreeOfPercision){
	var degreeOfPercision = degreeOfPercision.toLowerCase() || 'milliseconds';
		
	date1 = this.convertToDate(date1);
	date2 = this.convertToDate(date2);
		
	var comparison;
	
	comparison = this.compareYear(date1, date2);
	
	if(degreeOfPercision === 'year' || comparison !== 0){
		return comparison;
	};
	
	comparison = this.compareMonth(date1, date2);
	
	if(degreeOfPercision === 'month' || comparison !== 0){
		return comparison;
	};
	
	comparison = this.compareDate(date1, date2);
	
	if(degreeOfPercision === 'date' || comparison !== 0){
		return comparison;
	};
	
	comparison = this.compareHour(date1, date2);
	
	if(degreeOfPercision === 'hour' || comparison !== 0){
		return comparison;
	};

	comparison = this.compareMinute(date1, date2);
	
	if(degreeOfPercision === 'minute' || comparison !== 0){
		return comparison;
	};

	comparison = this.compareSecond(date1, date2);
	
	if(degreeOfPercision === 'second' || comparison !== 0){
		return comparison;
	};
	
	comparison = this.compareMillisecond(date1, date2);
	
	if(degreeOfPercision === 'millisecond' || comparison !== 0){
		return comparison;
	};
	
};

// Compares two integers and returns 1, 0, -1
Timetime.prototype.compare = function(int1, int2){
	
	if(int1 > int2){
		return 1;
	}else if(int1 < int2){
		return -1;
	}else if(int1 === int2){
		return 0;
	};
};

// Compares two Date()'s, date strings or Milliseconds and returns 1, 0 ,-1
Timetime.prototype.compareYear = function compareYear(date1, date2){
	date1 = this.convertToDate(date1);
	date2 = this.convertToDate(date2);
	
	return this.compare( date1.getFullYear(), date2.getFullYear() );
};

// Compares two Date()'s, date strings or Milliseconds and returns 1, 0 ,-1
Timetime.prototype.compareMonth = function compareMonth(date1, date2){
	date1 = this.convertToDate(date1);
	date2 = this.convertToDate(date2);

	return this.compare( date1.getMonth(), date2.getMonth() );
};

// Compares two Date()'s, date strings or Milliseconds and returns 1, 0 ,-1
Timetime.prototype.compareDate = function compareDate(date1, date2){
	date1 = this.convertToDate(date1);
	date2 = this.convertToDate(date2);

	return this.compare( date1.getDate(), date2.getDate() );
};

// Compares two Date()'s, date strings or Milliseconds and returns 1, 0 ,-1
Timetime.prototype.compareHour = function compareHour(date1, date2){
	date1 = this.convertToDate(date1);
	date2 = this.convertToDate(date2);

	return this.compare( date1.getHours(), date2.getHours() );
};

// Compares two Date()'s, date strings or Milliseconds and returns 1, 0 ,-1
Timetime.prototype.compareMinute = function compareMinute(date1, date2){
	date1 = this.convertToDate(date1);
	date2 = this.convertToDate(date2);

	return this.compare( date1.getMinutes(), date2.getMinutes() );
};

// Compares two Date()'s, date strings or Milliseconds and returns 1, 0 ,-1
Timetime.prototype.compareSecond = function compareSecond(date1, date2){
	date1 = this.convertToDate(date1);
	date2 = this.convertToDate(date2);

	return this.compare( date1.getSeconds(), date2.getSeconds() );
};

// Compares two Date()'s, date strings or Milliseconds and returns 1, 0 ,-1
Timetime.prototype.compareMillisecond = function compareMillisecond(date1, date2){
	date1 = this.convertToDate(date1);
	date2 = this.convertToDate(date2);

	return this.compare( date1.getMilliseconds(), date2.getMilliseconds() );
};

// Determines if Date(), date string or Milliseconds was yesterday.
Timetime.prototype.isYesterday = function(then){
	var now = new Date();
	then = this.convertToDate(then);
	
	if(now.getMonth() === then.getMonth() &&
			now.getYear() === then.getYear() &&
			now.getDate() === then.getDate()+1 ){
		return true;
	}else{
		return false;
	};
};

// Compares two Date()'s, date strings or Milliseconds and returns the number
// of whole days between them.
Timetime.prototype.daysDifference = function(date1, date2){
	var delta = this.getDelta(date1, date2);
	
	if(delta < 0){
		// Math.floor fuggles negative numbers.
		return Math.floor( (delta*-1) / this.TIME_IN_DAY ) * -1;
	}else{
		return Math.floor(delta/this.TIME_IN_DAY);
	};
};



// known good point ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^



Timetime.prototype.timeFrom2 = function(date1, date2, returnType){
		
	var isPast = false;
		
	if(date1 > date2 ){
		var tmp = date1;
		date1 = date2;
		date2 = tmp;
		console.log('a')
		isFuture = true;
	}
		
	console.log(date1, date2)
		
	var returnType = returnType || 'fancytime';
	var returnType = returnType.toLowerCase(); 
	
	var delta = Math.floor( (date1-date2) );
	
	console.log(delta)
	
	switch(returnType){
		case 'fancytime':		
			return this.fancifyTime(delta);
			
		case 'fancydate':		
			return this.fancifyDate(delta);

		case 'years':
			return Math.floor(delta/this.TIME_IN_YEAR);

		case 'months':
			return Math.floor(delta/this.TIME_IN_MONTH);

		case 'weeks':
			return Math.floor(delta/this.TIME_IN_WEEK);

		case 'days':
			return Math.floor(delta/this.TIME_IN_DAY);

		case 'hours':
			return Math.floor(delta/this.TIME_IN_HOUR);

		case 'minutes':
			return Math.floor(delta/this.TIME_IN_MINUTE);

		case 'seconds':
			return Math.floor(delta/this.TIME_IN_SECOND);

		default:
			return Math.floor(delta);
	}
};













Timetime.prototype.fancifyTime = function(miliseconds){
	
	function getPluralString(int){
		if(int > 1){
			return 's';
		}
		return '';
	}

	var ageString = '';

	// TODO: handle negative values so future/past don't matter

	if(miliseconds > this.TIME_IN_YEAR){

		var years = Math.floor(miliseconds/this.TIME_IN_YEAR);
		ageString = years + ' year'+getPluralString(years);

	}else if(miliseconds > this.TIME_IN_MONTH){

		var months = Math.floor(miliseconds/this.TIME_IN_MONTH);
		ageString = months + ' month'+getPluralString(months);

	}else if(miliseconds > this.TIME_IN_WEEK){

		var weeks = Math.floor(miliseconds/this.TIME_IN_WEEK);
		ageString = weeks + ' week'+getPluralString(weeks);

	}else if(miliseconds > this.TIME_IN_DAY){

		var days = Math.floor(miliseconds/this.TIME_IN_DAY);
		ageString = days + ' day'+getPluralString(days);

	}else if(miliseconds > this.TIME_IN_HOUR){

		var hours = Math.floor(miliseconds/this.TIME_IN_HOUR);
		ageString = hours + ' hour'+getPluralString(hours);

	}else if(miliseconds > this.TIME_IN_MINUTE){

		var minutes = Math.floor(miliseconds/this.TIME_IN_MINUTE);
		ageString = minutes + ' minute'+getPluralString(minutes);

	}else{
		// FIXME: resulting in NaN seconds???
		var seconds = Math.floor(miliseconds/this.TIME_IN_SECONDS);
		ageString = seconds + ' second'+getPluralString(seconds);

	}

	return ageString;
};











